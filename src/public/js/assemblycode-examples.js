const assembly = `
lw	1, 0, a		# r1 <- a
lui	2, 0x8000	# r2 <- 1000 0000
nand	3, 1, 2		# r3 <- a NAND 10000000
addi	2, 0, -1	# r2 <- 1111 1111
bne	3, 2, next	# if (a < 0) skip to next
sw	1, 0, b		# b = a
--------- SKIP ---------
lw	1, 0, a		# r1 <- a
lui	2, 0x8000	# r2 <- 1000 0000
nand	3, 1, 2		# r3 <- a NAND 10000000
addi	2, 0, -1	# r2 <- 1111 1111
bne	3, 2, next	# if (a < 0) skip to next
nand	1, 1, 1		# r1 <- a NAND a (flips bits)
addi	1, 1, 1		# r1 <- -a (adds one to the complement)
sw	1, 0, b		# b = -a
--------- SKIP ---------
lw	1, 0, a		# r1 <- a
lw	2, 0, b		# r2 <- b
bne	1, 2, loop	# if (a != b) enter loop
jump	out		# otherwise exit loop
loop:	add	1, 1, 1		# a*=2
add	1, 1, 1		# a*=4
bne	1, 2, loop	# if (a != b) continue loop
out:	...
--------- SKIP ---------
--------- SKIP ---------
--------- SKIP ---------
    lw	1, 0, a		# r1 <- a
    lw	2, 0, b		# r2 <- b
    bne	1, 2, notequal	# if (a == b) skip to equal

equal:	lw	1, 0, c		# r1 <- c
    addi	1, 1, 1		# c += 1
    sw	1, 0, c		# store c
    jump	next
--------- SKIP ---------
    notequal:	nand	2, 2, 2		# r2 <- b NAND b (flips bits)
    addi	2, 2, 1		# r2 <- -b
    add	3, 1, 2		# r3 <- a - b
    lui	4, 0x8000	# r4 <- 1000 0000
    nand	3, 3, 4		# r3 <- (a - b) NAND 10000000
    addi	4, 0, -1	# r4 <- 1111 1111
    bne	3, 4, aLTb	# if (a - b) < 0 skip to aLTb
--------- SKIP ---------
aGTb:	sw	2, 0, a		# a = -b
    jump	 next		# goto next
--------- SKIP ---------
aLTb:	nand	1, 1, 1		# r1 <- a NAND a (flips bits)
    addi	1, 1, 1		# r1 <- -a
    sw	1, 0, b		# b = -a
--------- SKIP ---------
next:	...`;