function resizeCanvasAndResults(dimensions, canvas, results) {
    const { width, height } = dimensions instanceof HTMLVideoElement
      ? faceapi.getMediaDimensions(dimensions)
      : dimensions
    canvas.width = width
    canvas.height = height
  
    // resize detections (and landmarks) in case displayed image is smaller than
    // original size
    return faceapi.resizeResults(results, { width, height })
  }
  
  function drawDetections(dimensions, canvas, detections) {
    console.log('drawing detections!');
    const resizedDetections = resizeCanvasAndResults(dimensions, canvas, detections)
    faceapi.drawDetection(canvas, resizedDetections)
  }
  
  function drawBeauty(dimensions, canvas, results, withBoxes = true) {
    const resizedResults = resizeCanvasAndResults(dimensions, canvas, results)
  
    if (withBoxes) {
      faceapi.drawDetection(canvas, resizedResults.map(det => det.detection))
    }
  
    const faceLandmarks = resizedResults.map(det => det.landmarks)
    const drawDotsOptions = {
      lineWidth: 2,
      drawLines: true,      
      color: 'green'
    }
    faceapi.drawLandmarks(canvas, faceLandmarks, drawDotsOptions)
    const drawLandmarksOptions = {
      lineWidth: 7,
      drawLines: false,      
      color: 'lightblue'
    }
    faceapi.drawLandmarks(canvas, faceLandmarks, drawLandmarksOptions)
  }

  function drawLandmarks(dimensions, canvas, results, withBoxes = true) {
    const resizedResults = resizeCanvasAndResults(dimensions, canvas, results)
  
    if (withBoxes) {
      faceapi.drawDetection(canvas, resizedResults.map(det => det.detection))
    }
  
    const faceLandmarks = resizedResults.map(det => det.landmarks)
    const drawLandmarksOptions = {
      lineWidth: 1,
      drawLines: true,
      color: 'white'
    }
    faceapi.drawLandmarks(canvas, faceLandmarks, drawLandmarksOptions)
  }

  function drawLandmarkDots(dimensions, canvas, results, withBoxes = true) {
    const resizedResults = resizeCanvasAndResults(dimensions, canvas, results)
  
    if (withBoxes) {
      faceapi.drawDetection(canvas, resizedResults.map(det => det.detection))
    }
  
    const faceLandmarks = resizedResults.map(det => det.landmarks)
    const drawLandmarksOptions = {
      lineWidth: 2,
      drawLines: false,
      color: 'green'
    }
    faceapi.drawLandmarks(canvas, faceLandmarks, drawLandmarksOptions)
  }

  function drawBoxes(dimensions, canvas, results) {
    const resizedResults = resizeCanvasAndResults(dimensions, canvas, results)    
    faceapi.drawDetection(canvas, resizedResults.map(det => det.detection), {
      withScore: false,
      boxColor: 'rgba(255, 0, 0, 0.75)',
      lineWidth: 5,
      textColor: 'white'
    });
  }

  function clearCanvas(canvas) {
    const context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
  }
  
  function drawExpressions(dimensions, canvas, results, thresh, withBoxes = true) {
    const resizedResults = resizeCanvasAndResults(dimensions, canvas, results)
  
    if (withBoxes) {
      faceapi.drawDetection(canvas, resizedResults.map(det => det.detection), { withScore: false })
    }
  
    faceapi.drawFaceExpressions(canvas, resizedResults.map(({ detection, expressions }) => ({ position: detection.box, expressions })))
  }