let videoEl;

let inputSize = 256
let scoreThreshold = 0.5
let trackedPerson;
let maleNames;
let femaleNames;

let ctx;

$(document).ready(async () => {
    console.log('DOM is ready!');
    videoEl = $('#webcamStream').get(0);

    ctx = $('#overlay').get(0).getContext('2d');

    console.log(ctx);
    particlesJS.load('particles-js', '/models/particlesjs-config.json', function() {
        console.log('callback - particles.js config loaded');
    });


    run();
});


let femaleRes;
let femaleMatch;
let maleRes;
let maleMatch;

const run = async () => {

    console.log('loading models')
    // await changeFaceDetector(TINY_FACE_DETECTOR);
    
    await faceapi.nets.tinyFaceDetector.load('/models/')
    await faceapi.nets.faceLandmark68Net.load('/models/')
    await faceapi.loadFaceLandmarkTinyModel('/models/');
    await faceapi.nets.faceRecognitionNet.load('/models/');
    await faceapi.loadFaceExpressionModel('/models/');    

    console.log('Getting stream')
    const stream = await navigator.mediaDevices.getUserMedia({
        video: {
            width: {min: 1280}, 
            height: {min: 720}
        }
    });
    console.log('Patching stream to video element');    
    videoEl.srcObject = stream;

    const opts = new faceapi.TinyFaceDetectorOptions({ inputSize, scoreThreshold })

    const femaleRefImage = document.createElement('img');
    femaleRefImage.src = 'images/female-ideal.jpg';
    femaleRefImage.onload = async () => {
        femaleRes = await faceapi.detectSingleFace(femaleRefImage, opts).withFaceLandmarks().withFaceDescriptor();
        femaleMatch = new faceapi.FaceMatcher(femaleRes);
    }

    const maleRefImage = document.createElement('img');
    maleRefImage.src = 'images/male-ideal.jpg';
    maleRefImage.onload = async () => {
        maleRes = await faceapi.detectSingleFace(maleRefImage, opts).withFaceLandmarks().withFaceDescriptor();
        maleMatch = new faceapi.FaceMatcher(maleRes);
    }    
}

let searchTimeout;
let queryTimeout;

const onPlay = async () => {    

    if(videoEl.paused || videoEl.ended ) {
        return setTimeout(() => {
            onPlay();
        });
    }    

    const opts = new faceapi.TinyFaceDetectorOptions({ inputSize, scoreThreshold })
    const ts = Date.now();
    const result = await faceapi.detectSingleFace(videoEl, opts).withFaceLandmarks(true).withFaceDescriptor();
    
    if(result) {            
     
        // calc beauty score baseret på descriptors
        calcBeautyByDescriptor(result);

        drawBeauty(videoEl, $('#overlay').get(0), [result], false);                

        if(maleRes && femaleRes) {
            console.log(maleRes, femaleRes);
    
            // check op mod female og male res
            const femaleMatchRes = femaleMatch.findBestMatch(result.descriptor);
            console.log("Female:",femaleMatchRes);                
            
            const maleMatchRes = maleMatch.findBestMatch(result.descriptor);
            console.log("Male:", maleMatchRes);       
            
            let baseScore;
            // find den laveste score
            if ( maleMatchRes._distance < femaleMatchRes._distance) {
                baseScore = maleMatchRes._distance;
            } else {
                baseScore = femaleMatchRes._distance;
            }

            let calculatedScore = ((1 - baseScore) * 10) + 4;
            if(calculatedScore > 10) {
                calculatedScore = 10;
            }

            if(calculatedScore < 6) {
                $('#beautyScore').css('color', 'red');                            
            } else {
                $('#beautyScore').css('color', 'green');
            }

            $('#beautyScore').html(calculatedScore.toFixed(2));
        }
        
        // console.log('Detection Results', result);
    } else {
        ctx.clearRect(0, 0, 1280, 720);
        $('#beautyScore').css('color', 'white');
        $('#beautyScore').html(0);
    }

    requestAnimationFrame(onPlay);    
}

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

function calcBeautyByDescriptor(res) {    
    const landmarks = res.landmarks._positions;
    console.log('LM', landmarks);
    // base distance er imellem 0 og 16
    const baseDist = Math.abs(distance(landmarks[0], landmarks[16]));
    console.log(baseDist);
    const noseLength = Math.abs(distance(landmarks[30],landmarks[27]));
    const noseRatio = baseDist / noseLength;
    console.log('Nose Length', noseLength, 'Nose Ratio', noseRatio);
    const eye1DistanceToNose = Math.abs(distance(landmarks[40], landmarks[28]));
    const eye2DistanceToNose = Math.abs(distance(landmarks[43], landmarks[28]));
    console.log(eye1DistanceToNose, eye2DistanceToNose);
}

const distance = (p1, p2) => {
    return Math.sqrt(Math.pow((p1._x - p2._x), 2) + Math.pow((p1._y - p2._y), 2))
}