const personalResultsData = {
    criminalRecord: {
        crimes: [
            " - Breaking and Entering",
            " - Economic Fraud",
            " - Driving under the Influence",
            " - Possesion of Controlled Substance",                        
            " - Possesion of Illegal Firearms",
            " - Possesion with intent to distribute",
            " - Drunk and disorderly",
            " - Public disturbance",
            " - Outstanding Parking Tickets",
            " - Assault and Battery"    
        ],
        legalStatus: ['None', 'Ex-Con', 'Wanted', 'Fugitive']
    },
    socialRecord: {
        ranks: ['Excellent Citizen', 'Good Citizen', 'Regular Citizen', 'Dissapointing Citizen', 'Bad Citizen'],
        positiveTraits: [
            "CO2 Neutral",
            "Honest",
            "Courageous",
            "Compassionate",
            "Law Abiding",
            "Patriotic",
            "Volunteer",
            "Self disciplined",
            "Healthy",
            "Employed"
        ],
        negativeTraits: [
            "Cheating",
            "Tax-evading",
            "Heavy use of social services",
            "Egotistic",
            "Abussive",
            "Unemployed"
        ]
    },
    economicRecord: {
        ranks: ["Good Economy", "Medium Economy", "Bad Economy"],
        debt: ["No Debt", "Some debt", "Heavy Debt"],
        incomes: ["High Income", "Medium Income", "Low Income", "Welfare"]

    }

}