let videoEl;

let inputSize = 512
let scoreThreshold = 0.5
let trackedPerson;
let maleNames;
let femaleNames;

let dataDisplayTime = 5;
let allowData = true;


$(document).ready(async () => {
    console.log('DOM is ready!');
    videoEl = $('#webcamStream').get(0);

    particlesJS.load('particles-js', '/models/particlesjs-config.json', function() {
        console.log('callback - particles.js config loaded');
    });

    run();

});


const run = async () => {

    console.log('loading models')
    // await changeFaceDetector(TINY_FACE_DETECTOR);
    
    await faceapi.nets.tinyFaceDetector.load('/models/')
    await faceapi.nets.faceLandmark68Net.load('/models/')
    await faceapi.nets.faceRecognitionNet.load('/models/');
    await faceapi.loadFaceExpressionModel('/models/');    

    console.log('Getting stream')
    const stream = await navigator.mediaDevices.getUserMedia({
        video: {
            width: {min: 1280}, 
            height: {min: 720}
        }
    });
    console.log('Patching stream to video element');    
    videoEl.srcObject = stream;
}

let searchTimeout;
let queryTimeout;

let timeoutInterval;

const onPlay = async () => {    

    if(videoEl.paused || videoEl.ended ) {
        return setTimeout(() => {
            onPlay();
        });
    }

    const opts = new faceapi.TinyFaceDetectorOptions({ inputSize, scoreThreshold })
    const ts = Date.now();
    const result = await faceapi.detectSingleFace(videoEl, opts).withFaceLandmarks().withFaceDescriptor();
    //const result = await faceapi.detectAllFaces(videoEl, opts).withFaceExpressions().withFaceLandmarks().withFaceDescriptors()
    
    if(result) {                               
        // draw tracking   
        drawBoxes(videoEl, $('#overlay').get(0), [result], true);        

        // hvis vi har lov til at vise data
        if(allowData == true) {
            allowData = false; 
            animateCSS("#searchingPanel", 'fadeOutDown', () => {
                $('#searchingPanel').css('opacity', 0);
            });

            const extractedRegion = [
                new faceapi.Rect(result.alignedRect._box._x, result.alignedRect._box._y, result.alignedRect._box._width, result.alignedRect._box._height)
            ];
            const canvases = await faceapi.extractFaces(videoEl, extractedRegion);            
            trackedFaceImage = canvases[0].toDataURL();                        

            const lastTracked = result;

            const checkintervatl = setInterval(async () => {
                // check om det face image vi tracker har en confidence score på over .9
                matchRes = await faceapi.detectSingleFace(videoEl, opts).withFaceLandmarks().withFaceDescriptor();
                if(matchRes) {
                    const matcher = new faceapi.FaceMatcher(matchRes);
                    const matchScore = matcher.findBestMatch(lastTracked.descriptor);
                    console.log('Match Score', matchScore);
                    if (matchScore._distance > 0.5) {
                        allowData = true;
                        clearInterval(checkintervatl);
                    } else {
                        allowData = false;
                    }
                }                
            }, dataDisplayTime * 1000);           

            trackedPerson = {};
            trackedPerson.res = [result];
            trackedPerson.profile = await getName('male');  

            animateCSS('#trackingDataPanel', 'fadeOutRight', () => {
                $('#trackinDataPanel').css("opacity", 0);                
                $('#tracked').html('');                
                animateCSS('#resultsPanel', 'fadeOutUp', () => {
                    $('#resultsPanel').css("opacity", 0);                    
                });
                animateCSS('#trackingDataPanel', 'fadeInRight', () => {
                    $('#trackingDataPanel').css("opacity", 1);
                    $('#tracked').html(`<p>Subject name: <b class="blurry-text">${trackedPerson.profile}</b> <img class="trackedFace" src="${trackedFaceImage}"></p>`);                           
                    $('#tracked').css("opacity", 1);
                    $('#tracked').css("display", 'block');
                    queryTimeout = setTimeout(() => {
                        $('#searchingStatus').remove();
                        $('#tracked').append('<p>Subject data search complete</p>')

                        const queryData = calculateScore();
                        console.log(queryData);
                        const criminalHtml = `
                        <span class="font-1">Score: ${queryData.crimeScore}/100</span><br>                    
                        Legal Status: <b>${queryData.legalStatus}</b><br>
                        <b style="text-decoration: underline;">Crimes:</b> <br>
                        ${queryData.crimes}
                        `;                        ;
                        $('#criminalRecord').html(criminalHtml);

                        const socialHtml = `
                        <span class="font-1">Score: ${queryData.socialScore}/100</span><br>
                        Social Rank: <b>${queryData.socialRank}</b><br>
                        <b style="text-decoration: underline;">Positive Traits:</b><br>
                        ${queryData.positiveTraits}<br>
                        <b style="text-decoration: underline;">Negative Traits:</b> <br>
                        ${queryData.negativeTraits}<br>
                        `;

                        $('#socialRecord').html(socialHtml);
                        console.log('qd', queryData);
                        const economicHtml = `
                        <span class="font-1">Economic Rank: ${queryData.economicRank}</span><br>
                        Income: <b>${queryData.income}</b></br>
                        Debt: <b>${queryData.debt}</b><br>
                        `;

                        $('#economicRecord').html(economicHtml);

                        animateCSS('#resultsPanel', 'fadeInDown', () => {
                            $('#resultsPanel').css("opacity", 1);                            
                        });
                    }, 500);
                });
            });
        }
        
    } else {
        // clear drawing
        clearCanvas($('#overlay').get(0));        
        if($('#searchingPanel').css('opacity') == 0) {            
            animateCSS('#searchingPanel', 'fadeInUp', () => {
                $('#searchingPanel').css('opacity', 1);
            });            
        }
    }

    setTimeout(() => {
        onPlay();
    });
}

const getName = async (gender) => {
    let name;
    let randomIndex = Math.floor(Math.random() * 18) + 10;
    console.log('Using randomindex', randomIndex);
    name = randomString(randomIndex);
    return name;
}

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

const calculateScore = () => {
    const returnObj = {}
    const options = personalResultsData;
    // calculate crime score
    const crimeScore = Math.floor(Math.random() * 100);
    let legalStatus;    
    let crimes = [];
    if  (crimeScore < 25) {
        legalStatus = '<span style="color: green;"><b>' + options.criminalRecord.legalStatus[0] + '</b></span>';
        crimes = 'None';
    } else if (crimeScore < 65) {
        legalStatus = options.criminalRecord.legalStatus[1];
        const crimeIndex = Math.floor(Math.random() * (options.criminalRecord.crimes.length - 1));
        crimes = options.criminalRecord.crimes[crimeIndex] + ' <span style="color: #4292f4;"><b>[time served]</b></span>';
    } else if (crimeScore < 90) {
        legalStatus = '<b><span style="color: #ffb732;">' + options.criminalRecord.legalStatus[2] + '</b></span>';
        const crimeIndex = Math.floor(Math.random() * (options.criminalRecord.crimes.length - 1));
        let crimeIndex2 = Math.floor(Math.random() * (options.criminalRecord.crimes.length - 1));
        while(crimeIndex2 == crimeIndex) {
            crimeIndex2 = Math.floor(Math.random() * (options.criminalRecord.crimes.length - 1));
        }
        crimes = options.criminalRecord.crimes[crimeIndex] + '<br>';
        crimes += options.criminalRecord.crimes[crimeIndex2];
    } else {
        legalStatus = '<span class="animated infinte pulse" style="color: red;"><b>' + options.criminalRecord.legalStatus[3] + '</b></span>';
        const crimeIndex = Math.floor(Math.random() * (options.criminalRecord.crimes.length - 1));
        let crimeIndex2 = Math.floor(Math.random() * (options.criminalRecord.crimes.length - 1));
        let crimeIndex3 = Math.floor(Math.random() * (options.criminalRecord.crimes.length - 1));
        let crimeIndex4 = Math.floor(Math.random() * (options.criminalRecord.crimes.length - 1));
        
        crimes = options.criminalRecord.crimes[crimeIndex] + '<br>';
        crimes += options.criminalRecord.crimes[crimeIndex2] + '<br>';
        crimes += options.criminalRecord.crimes[crimeIndex3] + '<br>';
        crimes += options.criminalRecord.crimes[crimeIndex4];
    }

    returnObj.crimes = crimes;
    returnObj.legalStatus = legalStatus;
    returnObj.crimeScore = crimeScore;

    // social score
    const socialScore = 100 - crimeScore;
    let socialRank;
    let negativeTraits;
    let positiveTraits;
    if(socialScore < 20) {
        socialRank = '<span style="color: red;">' + options.socialRecord.ranks[4] + '</span>';
        let negTraitIndex = Math.floor(Math.random() * (options.socialRecord.negativeTraits.length - 1));
        let negTraitIndex2 = Math.floor(Math.random() * (options.socialRecord.negativeTraits.length - 1));
        while (negTraitIndex == negTraitIndex2){
            negTraitIndex2 = Math.floor(Math.random() * (options.socialRecord.negativeTraits.length - 1));
        }
        negativeTraits = options.socialRecord.negativeTraits[negTraitIndex] + '<br>'; 
        negativeTraits += options.socialRecord.negativeTraits[negTraitIndex2];
        positiveTraits = '';
    } else if(socialScore < 40) {
        socialRank = '<span style="color: #ffb732;">' + options.socialRecord.ranks[3] + '</span>';
        let negTraitIndex = Math.floor(Math.random() * (options.socialRecord.negativeTraits.length - 1));        
        negativeTraits = options.socialRecord.negativeTraits[negTraitIndex];         
        positiveTraits = '';
    } else if(socialScore < 80) {
        socialRank = '<span style="color: white;">' + options.socialRecord.ranks[2] + '</span>';    
        let posTraitIndex = Math.floor(Math.random() * (options.socialRecord.positiveTraits.length - 1));        
        positiveTraits = options.socialRecord.positiveTraits[posTraitIndex];         
        let negTraitIndex = Math.floor(Math.random() * (options.socialRecord.negativeTraits.length - 1));        
        negativeTraits = options.socialRecord.negativeTraits[negTraitIndex];         
    } else if(socialScore < 90) {
        socialRank = '<span style="color: #4292f4;">' + options.socialRecord.ranks[1] + '</span>';
        let posTraitIndex = Math.floor(Math.random() * (options.socialRecord.positiveTraits.length - 1));        
        positiveTraits = options.socialRecord.positiveTraits[posTraitIndex];   
        negativeTraits = '';
    } else {
        socialRank = '<span style="color: green;">' +options.socialRecord.ranks[0] + '</span>';
        let posTraitIndex = Math.floor(Math.random() * (options.socialRecord.positiveTraits.length - 1));
        let posTraitIndex2 = Math.floor(Math.random() * (options.socialRecord.positiveTraits.length - 1));
        while (posTraitIndex == posTraitIndex2){
            posTraitIndex2 = Math.floor(Math.random() * (options.socialRecord.positiveTraits.length - 1));
        }
        negativeTraits = '';
        positiveTraits = options.socialRecord.negativeTraits[posTraitIndex] + '<br>'; 
        positiveTraits += options.socialRecord.negativeTraits[posTraitIndex2];
    }

    returnObj.socialScore = socialScore;
    returnObj.socialRank = socialRank;
    returnObj.positiveTraits = '<span style="color: green;">' + positiveTraits + '</span>';
    returnObj.negativeTraits = '<span style="color: red;">' + negativeTraits + '</span>';

    console.log('Score options', options);

    let economicRank;
    let debt;
    let income;
    // economics
    if(socialScore > 90) {
        returnObj.income = options.economicRecord.incomes[0];
        returnObj.debt = options.economicRecord.debt[0];
        returnObj.economicRank = options.economicRecord.ranks[0];
    } else {
        if(socialScore < 50 && crimeScore > 70) {
            returnObj.income = '<span style="color: #ffb732;">' + options.economicRecord.incomes[3] + '</span>';
            returnObj.debt = '<span style="color: red;">' +  options.economicRecord.debt[2] + '</span>';
            returnObj.economicRank = options.economicRecord.ranks[2];
        } else {
            // randomize
            const rankIndex = Math.floor(Math.random() * 2);
            const debtIndex = Math.floor(Math.random() * 2);
            const incomeIndex = Math.floor(Math.random() * 3);
            let rankColor = 'white';
            let debtColor = 'white';
            let incomeColor = 'white';

            if(rankIndex == 0) {
                rankColor = 'green';
            } else if(rankIndex == 2) {
                rankColor = 'red';
            }

            if(incomeIndex == 0) {
                incomeColor = 'green';
            } else if( incomeIndex == 2) {
                incomeColor = '#ffb732';
            } else if( incomeIndex == 3) {
                incomeColor = 'red';
            }

            if(debtIndex == 2) {
                debtColor = 'red';
            }

            returnObj.income = '<span style="color: '+incomeColor+';">' + options.economicRecord.incomes[rankIndex] + '</span>';
            returnObj.debt = '<span style="color: '+debtColor+';">' + options.economicRecord.debt[debtIndex] + '</span>';
            returnObj.economicRank = '<span style="color: '+rankColor+';">' + options.economicRecord.ranks[incomeIndex] + '</span>';
        }
    }
 
    return returnObj;
}

function randomString(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }