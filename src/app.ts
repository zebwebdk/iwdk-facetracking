import express from 'express';

const app = express();

const port = 3000;

app.get('/', (req: express.Request, res: express.Response) => {
    console.log('Got root');
    res.json("OK");
});

app.listen(port, () => {
    console.log('Server is up on port', port);
});
